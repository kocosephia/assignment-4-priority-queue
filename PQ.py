import sys

class PriorityQueue (object):
    def __init__(self):
        self.priority = {}
        self.queue = []
        self.sortedQueue = []
        self.highestPriority = 0
        
    def push(self, nameInput, priorityInput):
        if priorityInput not in self.priority:
            self.priority[priorityInput] = []
        self.priority[priorityInput].append(nameInput)

    def pop(self):
        self.priority[self.highestPriority] = None

    def printOut_imp(self):
        for x in self.priority:
            for i in reversed(self.priority[x]):
                self.sortedQueue.insert(0,i)
    def printOut(self):
        self.printOut_imp()
        for x in  self.sortedQueue:
            print x
    




theQueue = PriorityQueue()

userInput = "test"

while userInput != "":
    userInput = raw_input("")
    if userInput != "":
        userString = str(userInput)
        userList = userString.split()
        userName = str(userList[0])
        userPriority = int(userList[1])
        theQueue.push(userName, userPriority)
    
theQueue.printOut()


        
